#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <malloc.h>

#define ARRAY_LENGTH(array) sizeof(array) / sizeof(int)

static const int CHECK_SPOT_CODES[] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024};
static const int PARITY_BIT_PLACEHOLDER = 999;

int getParityBitCodeLen(int codeLen) { // additional code length computation for main info code
    if (codeLen < 0) { return 0; }

    int r = 0;
    while ((pow(2, r) <= (codeLen + r + 1))) {
        r++;
    }
    return r;
}

static bool isValueCanBeUsedForParityCalc(int checkCodeIndex, int infoCodeIndex) {
    bool isIndexMatches = false;

    while(infoCodeIndex > 0) {
        int i = 0;
        while (true) {
            int temp = (int) pow(2, i);

            if (temp == infoCodeIndex) {
                break;
            } else if (temp > infoCodeIndex) {
                i--;
                break;
            }
            i++;
        }

//        printf("Max value pow of two: %d\n", i);
        int place = (int) pow(2, i);

        if (place == checkCodeIndex) {
            isIndexMatches = true;
            break;
        }
        infoCodeIndex -= place;
    }
    return isIndexMatches;
}

static void insertParityBitsToCodeArray(const int *code, int *resultArray, int resultArrayLength) {   //Enters placeholders for check code
    bool control = false;
    int index = 0;
    for (int i = 0; i < resultArrayLength; i++) {     // Example: 999, 999, 1, 999, 0, 0, 1, 999, 1, 0, 1

        for (int j = 0; j < ARRAY_LENGTH(CHECK_SPOT_CODES); j++) {
            if (i == (CHECK_SPOT_CODES[j] - 1)) {
                resultArray[i] = PARITY_BIT_PLACEHOLDER;
                control = true;
                index++;
                break;
            }
        }

        if (control == true) {
            control = false;
            continue;
        } else {
            resultArray[i] = code[i - index];
        }
    }
}

static void calculateParityBitValues(const int *resultArray, int **finalArray, int resultArrayLength) {
    for (int i = 0; i < resultArrayLength; i++) {
        (*finalArray)[i] = resultArray[i];
    }

    for (int i = 0; i < resultArrayLength; i++) {
        if (resultArray[i] == PARITY_BIT_PLACEHOLDER) {
            unsigned val = 0;

            for (int j = 0; j < resultArrayLength; j++) {       //Find and compute all sum for check code
                if (resultArray[j] != PARITY_BIT_PLACEHOLDER) {
                    if (isValueCanBeUsedForParityCalc(i + 1, j + 1)) {
                        unsigned iVal = resultArray[j];
                        val = val ^ iVal;
                    }
                }
            }

            (*finalArray)[i] = (int) val;
        } else {
            (*finalArray)[i] = resultArray[i];
        }
    }

}

static bool validateInputCode(const int *code, int length) {
    int maxLength = CHECK_SPOT_CODES[ARRAY_LENGTH(CHECK_SPOT_CODES) - 1];
    if (length > maxLength) {
        printf("Code is not valid. Too long. Max length allowed: %i\n", maxLength);
        return false;
    }

    for (int i = 0; i < length; ++i) {
        if (code[i] < 0 || code[i] > 1) {
            printf("Code is not valid. Only 1 and 0 is allowed\n");
            return false;
        }
    }
    return true;
}

void generateHemmingCode(int *code, int length, int **dest) {
    if (!validateInputCode(code, length)) {
        return;
    }
    int parityBitLen = getParityBitCodeLen(length);
    printf("Number or parity bits: %i\n", parityBitLen);
    int resultArrayLength = length + parityBitLen;
    printf("Length of code word: %i\n", resultArrayLength);
    int arrayWithParityPlaceholders[resultArrayLength];
    insertParityBitsToCodeArray(code, arrayWithParityPlaceholders, resultArrayLength);

    free(*dest);
    *dest = malloc(resultArrayLength * sizeof(int));
    if (*dest == NULL) {
        return;
    }
    calculateParityBitValues(arrayWithParityPlaceholders, dest, resultArrayLength);
}

int main() {        //HEMMING
    int code[] = {1,0,0,1,1,0,1,0}; //Input codes:     1) 1, 0, 0, 1, 1, 0, 1      2) 1, 0, 0, 1, 1, 0, 1 ,0
    int codeLength = ARRAY_LENGTH(code);
    int *dest;
    generateHemmingCode(code, codeLength, &dest);

    int totalCodeLength = codeLength + getParityBitCodeLen(codeLength);
    for (int i = 0; i < totalCodeLength; i++) {   //Should be: 1) 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1      2) 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0
        printf("%i, ", dest[i]);
    }
    free(dest);

//    printf("Lib test %i\n", addTwoNumbers(1, 2));

    return 0;
}